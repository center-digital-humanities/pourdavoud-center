<?php
/*
 Template Name: Projects Page
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">

					<h1 class="archive-title">
						<?php the_title(); ?>
					</h1>
					<?php the_content(); ?>
					<ul class="program-list">
					<?php $projects_loop = new WP_Query( 
						array( 'post_type' => 'projects', 'orderby' => 'title', 'order' => 'asc', 'posts_per_page' => -1
						));
					?>
					<?php if ( $projects_loop->have_posts() ) : while ( $projects_loop->have_posts() ) : $projects_loop->the_post(); ?>						
						<li class="program">
							<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'content-width' ); ?></a>
							<a href="<?php the_permalink() ?>"><h2><?php the_title(); ?></h2></a>
							<p>
								<?php $content = get_the_content();
								$trimmed_content = wp_trim_words( $content, 40, '...' );
								echo $trimmed_content; ?>
							</p>
							<a href="<?php the_permalink() ?>" class="btn">Continue Reading <span class="hidden"> about <?php the_title(); ?></span></a>
						</li>		
					<?php endwhile; ?>
					
					</ul>
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>