<?php
/*
 Template Name: Podcast Page
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<h1 class="archive-title">
						<?php the_title(); ?>
					</h1>
					<?php the_content(); ?>
					<ul class="podcast-list">
					<?php $podcast_loop = new WP_Query( 
						array( 'post_type' => 'podcast', 'posts_per_page' => -1
						));
					?>
					<?php if ( $podcast_loop->have_posts() ) : while ( $podcast_loop->have_posts() ) : $podcast_loop->the_post(); ?>
						<li class="podcast">
							<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'people-flex' ); ?></a>
							<div class="podcast-content">
								<a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
								<p>
									<strong>Recorded:</strong> <?php echo get_the_date(); ?><br />
									<?php if(get_field('event_title')) { ?>
										<strong>Event:</strong> <?php the_field('event_title'); ?><br />
									<? } ?>
									<?php if(get_field('speaker')) { ?>
										<br />by <?php the_field('speaker'); ?>
									<? } ?>
								</p>
								<a href="<?php the_permalink() ?>" class="btn">Read More</a>
							</div>
						</li>		
					<?php endwhile; ?>
					</ul>
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>
<?php get_footer(); ?>