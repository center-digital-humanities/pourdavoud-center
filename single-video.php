<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
						<section class="entry-content" itemprop="articleBody">
							<?php if(get_field('video_url')) { ?>
								<?php the_field('video_url'); ?>
							<? } ?>
							<p>
								<strong>Recorded:</strong> <?php echo get_the_date(); ?><br />
								<?php if(get_field('event_title')) { ?>
									<strong>Event:</strong> <?php the_field('event_title'); ?><br />
								<? } ?>
								<?php if(get_field('citation')) { ?>
								<strong>Citation:</strong> <?php the_field('citation'); ?><br />
								<? } ?>
								<?php if(get_field('speaker')) { ?>
									<br />by <?php the_field('speaker'); ?>
								<? } ?>
							</p>
							<?php the_content(); ?>
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class(); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>
				
				</div>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php								
								wp_nav_menu(array(
								   	'container' => false,
								   	'menu' => __( 'Videos', 'bonestheme' ),
								   	'menu_class' => 'video-nav',
								   	'theme_location' => 'video-nav',
								   	'before' => '',
								   	'after' => '',
								   	'depth' => 2,
								   	'items_wrap' => '<h3>Videos</h3> <ul>%3$s</ul>'
								));
							?>
						</nav>
					</div>
				</div>
			</div>

<?php get_footer(); ?>