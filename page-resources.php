<?php
/*
 Template Name: Resources Page
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">

					<h1 class="archive-title">
						<?php the_title(); ?>
					</h1>
					<?php the_content(); ?>
					<ul class="program-list">
					<?php $resources_loop = new WP_Query( 
						array( 'post_type' => 'resources', 'orderby' => 'title', 'order' => 'asc', 'posts_per_page' => -1
						));
					?>
					<?php if ( $resources_loop->have_posts() ) : while ( $resources_loop->have_posts() ) : $resources_loop->the_post(); ?>						
						<li class="program">
							<h2><?php the_title(); ?></h2>
							<?php the_post_thumbnail( 'content-width' ); ?>
				                            <?php if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) : ?>
				                            <div class="image-caption-container">
				                                <p class="caption"><?php echo $caption; ?></p>
				                            </div>
				                        <?php endif; ?>
											<?php the_content(); ?>
						<?php if(get_field('external_link')) { ?>
							<a href="<?php the_field('external_link'); ?>" class="btn">Visit Website <span class="hidden"> for <?php the_title(); ?></span></a>
						<?php } ?>
						</li>	
					<?php endwhile; ?>
					
					</ul>
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>