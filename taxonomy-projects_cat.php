<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">

					<h1 class="archive-title">
						<?php single_cat_title(); ?> Projects
					</h1>
					<ul class="program-list">
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
						<li class="program">
							<h2><a href="<?php the_field('resource_link'); ?>"><?php the_title(); ?></a></h2>
						<?php if(get_field('resource_type')) { ?>
							<span class="program-type"><?php the_field('resource_type'); ?></span>
						<?php } ?>
						<?php if(get_field('resource_email')) { ?>
							<span class="program-email"><strong>Email:</strong> <a href="mailto:<?php the_field('resource_email'); ?>"><?php the_field('resource_email'); ?></a>
							</span>
						<?php } ?>		
						<?php if(get_field('resource_phone_number')) { ?>
							<span class="program-phone"><strong>Phone:</strong> <?php the_field('resource_phone_number'); ?></span>
						<?php } ?>
							<?php the_content(); ?>
							<a href="<?php the_field('resource_link'); ?>" class="btn">Visit Website <span class="hidden"> for <?php echo $program_title; ?></span></a>
						</li>	
						
					<?php endwhile; ?>
					
					</ul>
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>