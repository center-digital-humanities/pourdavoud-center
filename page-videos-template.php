<?php
/*
 Template Name: Video Page
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<h1 class="archive-title">
						<?php the_title(); ?>
					</h1>
					<?php the_content(); ?>
					
					<?php 
						// By Date
						if( get_field('sort_by') == 'date' ) {
						    $video_loop = new WP_Query( 
								array( 'post_type' => 'video', 'posts_per_page' => -1
							)); ?>
							
							<ul class="video-list">
							
								<?php if ( $video_loop->have_posts() ) : while ( $video_loop->have_posts() ) : $video_loop->the_post(); ?>
									<li class="video">
										<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'people-flex' ); ?></a>
										<div class="video-content">
											<a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
											<p>
												<strong>Recorded:</strong> <?php echo get_the_date(); ?><br />
												<?php if(get_field('event_title')) { ?>
													<strong>Event:</strong> <?php the_field('event_title'); ?><br />
												<? } ?>
												<?php if(get_field('speaker')) { ?>
													<br />by <?php the_field('speaker'); ?>
												<? } ?>
											</p>
											<a href="<?php the_permalink() ?>" class="btn">Read More</a>
										</div>
									</li>		
								<?php endwhile; ?>
							</ul>
							<?php else : endif;
						}
						
						// By Event
						elseif( get_field('sort_by') == 'event' ) {
						    $terms = get_field('events');
							if( $terms ): ?>
								
							    <?php foreach( $terms as $term ): ?>
									<h2><?php echo esc_html( $term->name ); ?></h2>
									<ul class="video-list">
							    	<?php 
							    	$video_loop = new WP_Query( 
										array( 
											'post_type' => 'video', 'posts_per_page' => -1, 
											'tax_query' => array(
										        array(
										            'taxonomy' => 'event_cat',
										            'field'    => 'slug',
										            'terms'    => $term,
										        ),
									    	),
										)
									);
									
									if ( $video_loop->have_posts() ) : while ( $video_loop->have_posts() ) : $video_loop->the_post(); ?>
										<li class="video">
											<a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
											<p>
												<strong>Recorded:</strong> <?php echo get_the_date(); ?><br />
												<?php if(get_field('event_title')) { ?>
													<strong>Event:</strong> <?php the_field('event_title'); ?><br />
												<? } ?>
												<?php if(get_field('speaker')) { ?>
													<br />by <?php the_field('speaker'); ?>
												<? } ?>
											</p>
											<a href="<?php the_permalink() ?>" class="btn">Read More</a>
										</li>		
									<?php endwhile; 
								else : endif;
							    ?>
							    </ul>        
							        
							    <?php endforeach;
							endif;
						}
						
						// By Presenter
						elseif( get_field('sort_by') == 'presenter' ) {
							$cat_terms = get_terms(
				                array('speaker_cat'),
				                array(
			                        'hide_empty'    => false,
			                        'orderby'       => 'name',
			                        'order'         => 'ASC',
				                )
				            );
							
							if( $cat_terms ) :
							    foreach( $cat_terms as $term ) : ?>
									<h2><?php echo esc_html( $term->name ); ?></h2>
							        <ul class="video-list">
										<?php
										$video_loop = new WP_Query( 
							   		        $args = array(
												'post_type' => 'video', 'posts_per_page' => -1, 
												'tax_query' => array(
													array(
														'taxonomy' => 'speaker_cat',
														'field'    => 'slug',
														'terms'    => $term->slug,
													),
												),
								            )
								        );
										
										if ( $video_loop->have_posts() ) : while ( $video_loop->have_posts() ) : $video_loop->the_post(); ?>
											<li class="video">
												<a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
												<p>
													<strong>Recorded:</strong> <?php echo get_the_date(); ?><br />
													<?php if(get_field('event_title')) { ?>
														<strong>Event:</strong> <?php the_field('event_title'); ?><br />
													<? } ?>
													<?php if(get_field('speaker')) { ?>
														<br />by <?php the_field('speaker'); ?>
													<? } ?>
												</p>
												<a href="<?php the_permalink() ?>" class="btn">Read More</a>
											</li>	
							           <?php endwhile; 
									else : endif; ?>
									</ul>        
							        
							    <?php endforeach;
							endif;   
						}
						
						// By Time Period
						elseif( get_field('sort_by') == 'time' ) {
							$terms = get_field('time_period');
							if( $terms ): ?>
								
							    <?php foreach( $terms as $term ): ?>
									<h2><?php echo esc_html( $term->name ); ?></h2>
									<ul class="video-list">
							    	<?php 
							    	$video_loop = new WP_Query( 
										array( 
											'post_type' => 'video', 'posts_per_page' => -1, 
											'tax_query' => array(
										        array(
										            'taxonomy' => 'time_period_cat',
										            'field'    => 'slug',
										            'terms'    => $term,
										        ),
									    	),
										)
									);
									
									if ( $video_loop->have_posts() ) : while ( $video_loop->have_posts() ) : $video_loop->the_post(); ?>
										<li class="video">
											<a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
											<p>
												<strong>Recorded:</strong> <?php echo get_the_date(); ?><br />
												<?php if(get_field('event_title')) { ?>
													<strong>Event:</strong> <?php the_field('event_title'); ?><br />
												<? } ?>
												<?php if(get_field('speaker')) { ?>
													<br />by <?php the_field('speaker'); ?>
												<? } ?>
											</p>
											<a href="<?php the_permalink() ?>" class="btn">Read More</a>
										</li>		
									<?php endwhile; 
								else : endif;
							    ?>
							    </ul>        
							        
							    <?php endforeach;
							endif;
						}
					?>
					
					<?php bones_page_navi(); ?>

				</div>
				<?php get_sidebar(); ?>
			</div>
<?php get_footer(); ?>