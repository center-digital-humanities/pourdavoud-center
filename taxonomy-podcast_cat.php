<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">

					
					<h1 class="archive-title">
						<?php single_cat_title(); ?> Podcasts
					</h1>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>				

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
                        
								<?php // if there is a photo, use it
								if(get_field('speaker_photo')) {
									$image = get_field('speaker_photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-large';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
                        <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette 
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<dl>
									<dt class="name"><h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3></dt>
									<dd class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></dd>
									<dd class="section">
                                        <section class="entry-content cf">
                                            <?php the_post_thumbnail( 'content-width' ); ?> 
                                            <?php the_excerpt(); ?>
                                            <a href="<?php the_permalink() ?>" class="btn">Read More</a>
                                        </section>
									</dd>
								</dl>
						
					</article>

					<?php endwhile; ?>
					
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php								
                                // If an Media subpage
								//if (is_tree(2834) || get_field('menu_select') == "media") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Media', 'bonestheme' ),
									   	'menu_class' => 'media-nav',
									   	'theme_location' => 'media-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Media and Gallery</h3> <ul>%3$s</ul>'
									));
							//	}
								
							?>
						</nav>
					</div>
				</div>
                
			</div>

<?php get_footer(); ?>