				<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
				// Only run if The Events Calendar is installed 
				if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax() || tribe_is_upcoming() && is_tax()) { 
					// Do nothing	
				}
				// For posts			
				elseif (is_single() || is_category() || is_search() || is_archive()) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Grants & Fellowships subpage
								if (is_tree(1401) || get_field('menu_select') == "grants") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Grants & Fellowships', 'bonestheme' ),
										'menu_class' => 'grants-nav',
										'theme_location' => 'grants-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Grants & Fellowships</h3> <ul>%3$s</ul>'
									));
								}
								// If an Research subpage
								if (is_tree(950) || get_field('menu_select') == "research" || is_tax('resources_cat') || is_post_type_archive('resources') || is_singular ('resources') || is_tax('projects_cat') || is_post_type_archive('projects') || is_singular ('projects') || is_tree(2426) || is_tree(2432)) {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Research', 'bonestheme' ),
									   	'menu_class' => 'research-nav',
									   	'theme_location' => 'research-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Research</h3> <ul>%3$s</ul>'
									));
								}
								// If an People subpage
								if (is_tree(1476) || is_category('events', 'Lecture Series') || get_field('menu_select') == "people") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'People', 'bonestheme' ),
									   	'menu_class' => 'people-nav',
									   	'theme_location' => 'people-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>People</h3> <ul>%3$s</ul>'
									));
								}
								// If an Events subpage
								if (is_tree(1436) || get_field('menu_select') == "events") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Events', 'bonestheme' ),
									   	'menu_class' => 'events-nav',
									   	'theme_location' => 'events-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Events</h3> <ul>%3$s</ul>'
									));
								}
                                // If an Media subpage
								if (is_page_template( 'page-podcast-template.php' ) || get_field('menu_select') == "media") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Media', 'bonestheme' ),
									   	'menu_class' => 'media-nav',
									   	'theme_location' => 'media-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Media and Gallery</h3> <ul>%3$s</ul>'
									));
								}
								 // If a Video subpage
								if (is_page_template( 'page-videos-template.php' ) || is_singular ('video') ) {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Videos', 'bonestheme' ),
									   	'menu_class' => 'video-nav',
									   	'theme_location' => 'video-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Videos</h3> <ul>%3$s</ul>'
									));
								}
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if ( is_search() || is_404() || is_page('contact') || is_page('about') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
						<?php if ( is_page( 'give' )  ) : ?>
							<?php dynamic_sidebar( 'give-sidebar' ); ?>
						<?php else : ?>
						<?php endif; ?>
					</div>
				</div>
				<?php } ?>