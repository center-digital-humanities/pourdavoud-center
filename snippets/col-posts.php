<div class="col news-col <?php the_sub_field('posts_width'); ?>">
	<h3><?php the_sub_field('posts_title'); ?></h3>
	<?php if( get_sub_field('add_image_widget') ) {
		$widget_link = get_sub_field('widget_link');
		$widget_title = get_sub_field('widget_title');
		$widget_text = get_sub_field('widget_text');
		$widget_background_image = get_sub_field('widget_background_image');
		
		$bottom_widget_link = get_sub_field('bottom_widget_link');
		$bottom_widget_title = get_sub_field('bottom_widget_title');
		$bottom_widget_text = get_sub_field('bottom_widget_text');
		$bottom_widget_background_image = get_sub_field('bottom_widget_background_image');
		
		if(!empty($widget_background_image)): 
			// vars
			$url = $widget_background_image['url'];
			$title = $widget_background_image['title'];
			// thumbnail
			$size = 'content-width';
			$widget_image = $widget_background_image['sizes'][ $size ];
			$width = $widget_background_image['sizes'][ $size . '-width' ];
			$height = $widget_background_image['sizes'][ $size . '-height' ];
		endif;
		
		if(!empty($bottom_widget_background_image)): 
			// vars
			$url = $bottom_widget_background_image['url'];
			$title = $bottom_widget_background_image['title'];
			// thumbnail
			$size = 'content-width';
			$bottom_widget_image = $bottom_widget_background_image['sizes'][ $size ];
			$width = $bottom_widget_background_image['sizes'][ $size . '-width' ];
			$height = $bottom_widget_background_image['sizes'][ $size . '-height' ];
		endif;
    	
    	if( get_sub_field('widget_location') == 'top' || get_sub_field('widget_location') == 'both' ) { ?>
			<?php if( $widget_link ): ?>
				<a href="<?php echo esc_url( $widget_link ); ?>" class="widget-link">
			<?php endif; ?>
				<div class="image-widget" style="background-image: url('<?php echo esc_url( $widget_image ); ?>');">
					<?php if($widget_title || $widget_text): ?>
						<div class="image-content">
					<?php endif; ?>
						<?php if($widget_title): ?>
							<h4><?php echo $widget_title; ?></h4>
						<?php endif; ?>
						<?php if($widget_text): ?>
							<span><?php echo $widget_text; ?></span>
						<?php endif; ?>
					<?php if($widget_title || $widget_text): ?>
						</div>
					<?php endif; ?>
				</div>
			<?php if( $widget_link ): ?>
				</a>
			<?php endif; ?>
		<?php }
	} ?>
	
	<?php $term = get_sub_field('category');
		$amount = get_sub_field('amount_to_show');
		if (!empty($term)) {
			$posts_query = new WP_Query( array( 'showposts' => $amount, 'cat' => $term->term_id ) );
		} else {
			$posts_query = new WP_Query( array( 'showposts' => $amount) );
		}
	?>
	<ol class="<?php if(get_sub_field('show_categories') == "yes") { ?>categories<?php } if(get_sub_field('show_image') == "yes") { ?>images<?php }?>">
		<?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>
		<?php if(get_sub_field('show_categories') == "no") { ?><a href="<?php the_permalink() ?>"><?php } ?>
			<li>
				<?php 
					if(get_sub_field('posts_width') == "two") {
						if(get_sub_field('show_image') == "yes") {
							if(get_sub_field('show_categories') == "yes") { ?>
								<a href="<?php the_permalink() ?>">
							<?php }
								if ( has_post_thumbnail() ) {
									$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article-thumb' );
									$url = $thumb['0']; ?>
									<img src="<?=$url?>" alt="<?php the_title(); ?>" />
								<?php } else { ?>
							        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-thumb.jpg" alt="A photo of <?php the_title(); ?>" />
							<?php }
							if(get_sub_field('show_categories') == "yes") { ?>
								</a>
							<?php }
						}
					} 
				?>
				<div class="item">
				<?php if(get_sub_field('show_categories') == "yes") { ?>
					<div class="category-list">
						<span class="category"><?php the_category( ', ' ); ?></span>
					</div>
				<?php } ?>
					<?php if(get_sub_field('show_categories') == "yes") { ?><a href="<?php the_permalink() ?>"><?php } ?>
					<h4><?php the_title(); ?></h4>
					<p>
						<?php $content = get_the_content();
						$trimmed_content = wp_trim_words( $content, 27, '...' );
						echo $trimmed_content; ?>
					</p>
					<?php if(get_sub_field('show_categories') == "yes") { ?></a><?php } ?>
				</div>
			</li>
		<?php if(get_sub_field('show_categories') == "no") { ?></a><?php } ?>
		<?php endwhile; ?>
	</ol>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
	<?php if( $term ) { ?>
	<a class="btn" href="/category/<?php echo $term->slug; ?>/">View All<span class="hidden"> <?php echo $term->name; ?></span></a>
	<?php } else { ?>
	<a class="btn" href="/category/news/">View All<span class="hidden"> News</span></a>
	<?php } ?>
	
	<?php if( get_sub_field('add_image_widget') ) {
		if( get_sub_field('widget_location') == 'bottom' || get_sub_field('widget_location') == 'both' ) { ?>
			<?php if( $widget_link ): ?>
				<a href="<?php echo esc_url( $bottom_widget_link ); ?>" class="widget-link">
			<?php endif; ?>
				<div class="image-widget" style="background-image: url('<?php echo esc_url( $bottom_widget_image ); ?>');">
					<?php if($bottom_widget_title || $bottom_widget_text): ?>
						<div class="image-content">
					<?php endif; ?>
						<?php if($bottom_widget_title): ?>
							<h4><?php echo $bottom_widget_title; ?></h4>
						<?php endif; ?>
						<?php if($bottom_widget_text): ?>
							<span><?php echo $bottom_widget_text; ?></span>
						<?php endif; ?>
					<?php if($bottom_widget_title || $bottom_widget_text): ?>
						</div>
					<?php endif; ?>
				</div>
			<?php if( $bottom_widget_link ): ?>
				</a>
			<?php endif; ?>
		<?php } 
	} ?>
</div>