<div class="col events-col <?php the_sub_field('events_width'); ?>">
	<h3><?php the_sub_field('events_title'); ?></h3>
	<?php if( get_sub_field('add_image_widget') ) {
		$widget_link = get_sub_field('widget_link');
		$widget_title = get_sub_field('widget_title');
		$widget_text = get_sub_field('widget_text');
		$widget_background_image = get_sub_field('widget_background_image');
		
		$bottom_widget_link = get_sub_field('bottom_widget_link');
		$bottom_widget_title = get_sub_field('bottom_widget_title');
		$bottom_widget_text = get_sub_field('bottom_widget_text');
		$bottom_widget_background_image = get_sub_field('bottom_widget_background_image');
		
		if(!empty($widget_background_image)): 
			// vars
			$url = $widget_background_image['url'];
			$title = $widget_background_image['title'];
			// thumbnail
			$size = 'content-width';
			$widget_image = $widget_background_image['sizes'][ $size ];
			$width = $widget_background_image['sizes'][ $size . '-width' ];
			$height = $widget_background_image['sizes'][ $size . '-height' ];
		endif;
		
		if(!empty($bottom_widget_background_image)): 
			// vars
			$url = $bottom_widget_background_image['url'];
			$title = $bottom_widget_background_image['title'];
			// thumbnail
			$size = 'content-width';
			$bottom_widget_image = $bottom_widget_background_image['sizes'][ $size ];
			$width = $bottom_widget_background_image['sizes'][ $size . '-width' ];
			$height = $bottom_widget_background_image['sizes'][ $size . '-height' ];
		endif;
    	
    	if( get_sub_field('widget_location') == 'top' || get_sub_field('widget_location') == 'both' ) { ?>
			<?php if( $widget_link ): ?>
				<a href="<?php echo esc_url( $widget_link ); ?>" class="widget-link">
			<?php endif; ?>
				<div class="image-widget" style="background-image: url('<?php echo esc_url( $widget_image ); ?>');">
					<?php if($widget_title || $widget_text): ?>
						<div class="image-content">
					<?php endif; ?>
						<?php if($widget_title): ?>
							<h4><?php echo $widget_title; ?></h4>
						<?php endif; ?>
						<?php if($widget_text): ?>
							<span><?php echo $widget_text; ?></span>
						<?php endif; ?>
					<?php if($widget_title || $widget_text): ?>
						</div>
					<?php endif; ?>
				</div>
			<?php if( $widget_link ): ?>
				</a>
			<?php endif; ?>
		<?php }
	} ?>
	
	<?php $term = get_sub_field('category');
		$amount = get_sub_field('amount_to_show');
		if($term) { 
			$events_query = new WP_Query( array( 'post_type' => 'tribe_events', 'showposts' => $amount, 
			'tax_query' => array(
				array(
					'taxonomy' => 'tribe_events_cat',
					'terms' => array($term->term_id)
				)
			)
		) ); 
		} else {
			$events_query = new WP_Query( array( 'post_type' => 'tribe_events', 'showposts' => $amount ) );
		}
		?>
	<ol class="<?php if(get_sub_field('show_categories') == "yes") { ?>categories<?php } if(get_sub_field('show_image') == "yes") { ?>images<?php }?>">
		<?php if ($events_query->have_posts()) : while ($events_query->have_posts()) : $events_query->the_post(); ?>
		<li class="tribe-events-list-widget-events">
			<?php 
				if(get_sub_field('events_width') == "two") {
					if(get_sub_field('show_image') == "yes") {
						if ( has_post_thumbnail() ) {
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article-thumb' );
							$url = $thumb['0']; ?>
							<a href="<?php the_permalink() ?>"><img src="<?=$url?>" alt="<?php the_title(); ?>" /></a>
						<?php } else { ?>
					        <a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-thumb.jpg" alt="A photo of <?php the_title(); ?>" /></a>
					<?php }
					}
				} 
			?>
			<div class="item">
				<?php do_action( 'tribe_events_list_widget_before_the_event_title' ); ?>
				<!-- Event Title -->
				<?php
					if(get_sub_field('show_categories') == "yes") {
						$event_cat = wp_get_object_terms( $post->ID, 'tribe_events_cat' );
						if ( ! empty( $event_cat ) ) {
							if ( ! is_wp_error( $event_cat ) ) { ?>
								<div class="category-list">
									<?php foreach( $event_cat as $term ) {
										echo '<span class="category"><a href="' . get_term_link( $term->slug, 'tribe_events_cat' ) . '">' . esc_html( $term->name ) . '</a></span>'; 
									} ?>
								</div>
							<?php }
						}
					}
				?>					
				<a href="<?php the_permalink() ?>">
					<h4><?php the_title(); ?></h4>
					<?php do_action( 'tribe_events_list_widget_after_the_event_title' ); ?>
					<!-- Event Time -->
					<?php do_action( 'tribe_events_list_widget_before_the_meta' ) ?>
					<div class="duration">
						<?php echo tribe_events_event_schedule_details(); ?>
					</div>
					<?php do_action( 'tribe_events_list_widget_after_the_meta' ) ?>
				</a>
			</div>
		</li>
		<?php endwhile; ?>
	</ol>
	<?php else : ?>
	<p>There are no upcoming events. Please check back soon.</p>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
	<?php if( $term ) { ?>
	<a class="btn" href="/events/category/<?php echo $term->slug; ?>/">View All<span class="hidden"> <?php echo $term->name; ?></span></a>
	<?php } else { ?>
	<a class="btn" href="/events/">View All<span class="hidden"> Events</span></a>
	<?php } ?>
	
	<?php if( get_sub_field('add_image_widget') ) {
		if( get_sub_field('widget_location') == 'bottom' || get_sub_field('widget_location') == 'both' ) { ?>
			<?php if( $widget_link ): ?>
				<a href="<?php echo esc_url( $bottom_widget_link ); ?>" class="widget-link">
			<?php endif; ?>
				<div class="image-widget" style="background-image: url('<?php echo esc_url( $bottom_widget_image ); ?>');">
					<?php if($bottom_widget_title || $bottom_widget_text): ?>
						<div class="image-content">
					<?php endif; ?>
						<?php if($bottom_widget_title): ?>
							<h4><?php echo $bottom_widget_title; ?></h4>
						<?php endif; ?>
						<?php if($bottom_widget_text): ?>
							<span><?php echo $bottom_widget_text; ?></span>
						<?php endif; ?>
					<?php if($bottom_widget_title || $bottom_widget_text): ?>
						</div>
					<?php endif; ?>
				</div>
			<?php if( $bottom_widget_link ): ?>
				</a>
			<?php endif; ?>
		<?php } 
	} ?>
</div>