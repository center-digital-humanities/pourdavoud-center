<?php
// Video Post Type Settings

// add custom categories
register_taxonomy( 'event_cat', 
	array('video'), /* if you change the name of register_post_type( 'Resources', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Events', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Event', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Events', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Events', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Event', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Event:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Event', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Event', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Event', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Event Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'event_cat' )
	)
);

// add custom categories
register_taxonomy( 'speaker_cat', 
	array('video'), /* if you change the name of register_post_type( 'Resources', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Speakers', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Speaker', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Speakers', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Speakers', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Speaker', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Speaker:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Speaker', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Speaker', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Speaker', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Speaker Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'speaker' )
	)
);

// add custom categories
register_taxonomy( 'time_period_cat', 
	array('video'), /* if you change the name of register_post_type( 'Resources', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Time Periods', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Time Period', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Time Periods', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Time Periods', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Time Period', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Time Period:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Time Period', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Time Period', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Time Period', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Time Period Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'period' )
	)
);

// let's create the function for the custom type
function video_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'video', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Videos', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Video', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Videos', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Video', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Video', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Video', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Video', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Videos', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No Video added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'All videos for the department', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 0, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-video-alt2', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'video', 'with_front' => true ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'revisions', 'author', 'thumbnail', 'page-attributes' )
			
		) /* end of options */
	); /* end of register post type */
}

// adding the function to the Wordpress init
add_action( 'init', 'video_post_type');	
?>