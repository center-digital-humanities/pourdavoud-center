<?php
// Resources Post Type Settings

// add custom categories
register_taxonomy( 'resources_cat', 
	array('resources'), /* if you change the name of register_post_type( 'Resources', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Resource Categories', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Resource Category', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Resource Categories', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Resource Categories', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Resource Category', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Resource Category:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Resource Category', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Resource Category', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Resource Category', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Resource Category Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => false, //For now
		'query_var' => true,
		//'rewrite' => array( 'slug' => 'resources' )
	)
);

// let's create the function for the custom type
function resources_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'resources', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Resources', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Resource', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Resources', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Resource', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Resource', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Resource', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Resource', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Resources', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No Resources added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'All resources for department', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'has_archive' => false,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 5, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-media-archive', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'resources', 'with_front' => false ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'revisions', 'author', 'thumbnail' )
			
		) /* end of options */
	); /* end of register post type */
}

// adding the function to the Wordpress init
add_action( 'init', 'resources_post_type');	
?>