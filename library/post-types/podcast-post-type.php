<?php
// Podcast Post Type Settings

// let's create the function for the custom type
function Podcast_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'podcast', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Podcasts', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Podcast', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Podcast', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Podcast', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Podcast', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Podcast', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Podcast', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Podcast', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No Podcast added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'All podcast for department', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 0, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-format-audio', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'podcast', 'with_front' => true ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'revisions', 'author', 'thumbnail' )
			
		) /* end of options */
	); /* end of register post type */
}

// adding the function to the Wordpress init
add_action( 'init', 'podcast_post_type');	
?>